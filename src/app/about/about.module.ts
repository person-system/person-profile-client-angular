import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutRoutingModule } from './about-routing.module';
import { AboutSystemComponent } from './about-system/about-system.component';
import { AboutAuthorsComponent } from './about-authors/about-authors.component';


@NgModule({
  declarations: [AboutSystemComponent, AboutAuthorsComponent],
  imports: [
    CommonModule,
    AboutRoutingModule
  ]
})
export class AboutModule { }
