import { Component, OnInit } from '@angular/core';
import { AboutMainService } from '../about-main.service';

@Component({
  selector: 'app-about-system',
  templateUrl: './about-system.component.html',
  styleUrls: ['./about-system.component.css']
})
export class AboutSystemComponent implements OnInit {
  versionServer: string;

  constructor(private aboutService: AboutMainService) {}

  ngOnInit() {
    this.aboutService
      .getVersionServer()
      .subscribe(data => (this.versionServer = data.version));
  }
}
