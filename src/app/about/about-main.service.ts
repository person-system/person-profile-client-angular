import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment as env } from './../../environments/environment';
import { VersionDTO } from './models';

@Injectable({
  providedIn: 'root'
})
export class AboutMainService {
  constructor(private http: HttpClient) {}

  getVersionServer(): Observable<VersionDTO> {
    return this.http.get<VersionDTO>(`${env.API_URL}/about/version`);
  }

  getAuthors(): Array<any> {
    return [
      { name: 'Demo', email: 'demo@demo.com', role: 'owner' },
      { name: 'Demo2', email: 'demo2@demo.com', role: 'developer' }
    ];
  }
}
