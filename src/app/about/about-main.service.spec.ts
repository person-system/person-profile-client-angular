import { TestBed } from '@angular/core/testing';

import { AboutMainService } from './about-main.service';

describe('AboutMainService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AboutMainService = TestBed.get(AboutMainService);
    expect(service).toBeTruthy();
  });
});
