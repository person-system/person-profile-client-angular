import { Component, OnInit } from '@angular/core';
import { AboutMainService } from '../about-main.service';

@Component({
  selector: 'app-about-authors',
  templateUrl: './about-authors.component.html',
  styleUrls: ['./about-authors.component.css']
})
export class AboutAuthorsComponent implements OnInit {
  authors: Array<any>;

  constructor(private aboutService: AboutMainService) {}

  ngOnInit() {
    this.authors = this.aboutService.getAuthors();
  }

}
