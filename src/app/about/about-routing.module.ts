import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutSystemComponent } from './about-system/about-system.component';
import { AboutAuthorsComponent } from './about-authors/about-authors.component';


const routes: Routes = [
  {path: 'system', component: AboutSystemComponent},
  {path: 'authors', component: AboutAuthorsComponent},
  {path: '', redirectTo: 'system', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AboutRoutingModule { }
