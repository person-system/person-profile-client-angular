export interface UserDTO {
    login: string;
    personality_id: string;
    node_id: string;
}
