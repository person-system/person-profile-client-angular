import { PersonalityDTO } from './../models/personality.dto';
import { UserDTO } from './../models/user.dto';
import { NodeDTO } from './../models/node.dto';
import { ProfileMainService } from '../profile-main.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { tap, filter, map, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-profile-screen',
  templateUrl: './profile-screen.component.html',
  styleUrls: ['./profile-screen.component.css']
})
export class ProfileScreenComponent implements OnInit {
  personality$: Observable<PersonalityDTO>;
  user$: Observable<UserDTO>;
  node$: Observable<NodeDTO>;

  constructor(private personalityService: ProfileMainService) { }

  ngOnInit() {
    this.user$ = this.personalityService.getUser();
    // this.personality$ = this.personalityService.getPersonality();
    this.personality$ = this.user$.pipe(
      map((user: UserDTO) => user.personality_id),
      filter(id => id !== ''),
      switchMap(id => this.personalityService.getPersonality(id)),
      tap(console.log),
    );
    this.node$ = this.user$.pipe(
      tap(console.log),
      map((user: UserDTO) => user.node_id),
      filter(id => id !== ''),
      switchMap(id => this.personalityService.getNode(id)),
      tap(console.log),
    );
  }

}
