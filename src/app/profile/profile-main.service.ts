import { PersonalityDTO } from './models/personality.dto';
import { UserDTO } from './models/user.dto';
import { NodeDTO } from './models/node.dto';
import { Observable } from 'rxjs';
import { of } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment as env } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProfileMainService {

  constructor(private http: HttpClient) { }

  getPersonality(id: string): Observable<PersonalityDTO> {
    return this.http.get<PersonalityDTO>(`${env.API_URL}/personality/${id}`);
    // const personality = {name: 'Jone Doe', sex: 'MALE'};
    // return of(personality);
  }
  getUser(): Observable<UserDTO> {
    const user = {
      login: 'megauser',
      personality_id: '10000000-0000-0000-0000-000000000001',
      node_id: '00000000-0000-0000-0001-000000000001'
    };
    return of(user);
  }
  getNode(nid: string): Observable<NodeDTO> {
    let node: NodeDTO;
    if (nid === '00000000-0000-0000-0001-000000000001') {
      node = {id: '00000000-0000-0000-0001-000000000001', node_id: '00000000-0000-0000-0011-000000000001'};
    }
    return of(node);
  }
}
