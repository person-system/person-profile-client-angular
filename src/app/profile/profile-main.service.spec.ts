import { TestBed } from '@angular/core/testing';

import { ProfileMainService } from './profile-main.service';

describe('ProfileMainService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProfileMainService = TestBed.get(ProfileMainService);
    expect(service).toBeTruthy();
  });
});
