import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  { path: 'about', loadChildren: () => import(`./about/about.module`).then(m => m.AboutModule) },
  { path: 'settings', loadChildren: () => import(`./settings/settings.module`).then(m => m.SettingsModule) },
  { path: 'profile', loadChildren: () => import(`./profile/profile.module`).then(m => m.ProfileModule) },
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes),
    CommonModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
