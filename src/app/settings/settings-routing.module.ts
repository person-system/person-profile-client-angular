import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SettingsMainComponent } from './settings-main/settings-main.component';


const routes: Routes = [
  {path: 'main', component: SettingsMainComponent},
  {path: '', redirectTo: 'main', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule { }
