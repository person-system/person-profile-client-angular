import { TestBed } from '@angular/core/testing';

import { SettingsMainService } from './settings-main.service';

describe('SettingsMainService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SettingsMainService = TestBed.get(SettingsMainService);
    expect(service).toBeTruthy();
  });
});
