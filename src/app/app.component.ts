import { JwtService } from './jwt.service';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Персональный профиль';
  error: string;

  constructor(private jwtService: JwtService, private router: Router) { }

  public onLogin() {
    this.jwtService.getJWT()
      .pipe(first())
      .subscribe(
        result => this.router.navigate(['/profile']),
        err => {
          console.log(err);
        }
      );
  }

  logout() {
    this.jwtService.logout();
    this.router.navigate(['/']);
  }
}
